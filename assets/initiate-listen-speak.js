import { ListenSpeak as ListenSpeak }   from './listen-speak-class.js';

function initiateListenSpeak(){

    if(customElements.get("listen-speak") === undefined){
        customElements.define("listen-speak", ListenSpeak);
    }

}
export { initiateListenSpeak };