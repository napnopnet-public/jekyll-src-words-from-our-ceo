/**
 * creates download link for svg, png of jpg image file of a (Dutch) street name plate, created from a DOM element that renders a street name plate
 */
function createDownload() {
    /**
     *
     * @param newNode
     * @param referenceNode
     */
    function insertAfter(newNode, referenceNode) {
        //https://stackoverflow.com/questions/4793604/how-to-insert-an-element-after-another-element-in-javascript-without-using-a-lib
        referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
    }

    function getImageType() {
        var fileKind = document.getElementById("download-choice-id").value;
        if (fileKind === '') {
            if (!document.getElementById("download-choice-id").parentNode.contains(document.getElementById("chooser-id"))) {
                var chooser = document.getElementById("download-choice-id");
                chooser.style.borderColor = "red";
                var error_text = document.createTextNode(" << Kies eerst een bestandsformaat?");
                var error_span = document.createElement("span");
                error_span.setAttribute("id", "chooser-id");
                error_span.style.color = "red";
                error_span.append(error_text);
                insertAfter(error_span, chooser);
            }

        } else if (fileKind === 'dl_svg' || fileKind === 'dl_png' || fileKind === 'dl_jpg') {
            var cleanFileKind = fileKind.slice(3);
            createImageAndDownloadLink(cleanFileKind);
        } else {
            console.log('Sorry, we are out of luck');
        }
    }

    getImageType();

    /**
     * // https://github.com/tsayen/dom-to-image : js for creating svg, png and jpeg images from DOM
     */
    function createImageAndDownloadLink(type) {
        var toType = "to";
        toType += type.charAt(0).toUpperCase() + type.slice(1);
        if (type === "jpg") {
            toType = toType.slice(0, 4) + "e" + toType.slice(4);
        }

        var node = document.getElementById('stnameid');
        domtoimage[toType](node).then(function (dataUrl) {
            var img = new Image();
            img.src = dataUrl;
            img.setAttribute("id", "created-image");
            if (!!document.getElementById("created-image")) {
                document.getElementById("created-image").remove();
            }

            document.getElementById("image-result").appendChild(img);

            var link = document.createElement('a');
            link.setAttribute("id", "download-link");
            link.download = 'my-image-name.' + type;
            var linkteksts = document.createTextNode("download jouw straatnaambord");
            link.appendChild(linkteksts);
            link.href = dataUrl;
            if (!!document.getElementById("download-link")) {
                document.getElementById("download-link").remove();
            }

            document.getElementById("image-result").prepend(link);

        })
            .catch(function (error) {
                console.error('oops, something went wrong!', error);
            });
    }
}


/* event handler for Maak beeldbestand button */
document.getElementById("download_now").addEventListener("click", function (event) {
    event.preventDefault();
    createDownload();
});


/* events delegation event handler for form  */
document.getElementById("strNplate-form").addEventListener("change", function (event) {
    if (event.target.classList.contains("strName")) {
        document.getElementById("streetName").innerHTML = strip_tags(event.target.value, "<br>");
    }
    if (event.target.classList.contains("subtext")) {
        document.getElementById("subText").innerHTML = strip_tags(event.target.value, "<br>");
    }
    if (event.target.classList.contains("strNplate-em-range")) {
        document.getElementsByClassName('padder')[0].style.fontSize = (event.target.value / 100) + "em";
        document.getElementById("strNplate-em-number").value = event.target.value;
    }
    if (event.target.classList.contains("strNplate-em-size")) {
        document.getElementsByClassName('padder')[0].style.fontSize = (event.target.value / 100) + "em";
        document.getElementById("strNplate-em-range").value = event.target.value;
    }
    if (event.target.id === "download-choice-id") {
        document.getElementById("download-choice-id").style.borderColor = "green";
        if (document.getElementById("download-choice-id").parentNode.contains(document.getElementById("chooser-id"))) {
            document.getElementById("chooser-id").remove();
        }
    }
});