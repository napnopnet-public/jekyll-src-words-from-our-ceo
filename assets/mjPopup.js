function initiateMjPict(urlBasePath) {

    document.getElementById("de8f986bbbf3f90b732c82b631761136").addEventListener("click", function(event){

        if (event.target.parentElement.classList.contains("Michael_J")){
            event.preventDefault();

            playSoundFile(event.target.getAttribute("data-sound-src"));

            let thisAnchor = event.target.parentElement;
            let mjDiv = document.createElement('div');
            thisAnchor.prepend(mjDiv)
            mjDiv.classList.add('popupMJ')
            let mjBig = document.createElement("img");
            mjBig.classList.add('hide');
            mjDiv.append(mjBig);
            mjBig.src = event.target.getAttribute("data-bigpict-src");

            let blowupM = new Promise(function(resolve, reject){
                window.setTimeout(
                    function() {
                        mjBig.classList.add('show')
                        mjBig.classList.remove('hide')
                        resolve("ready")
                    }, 1000);

            });

            blowupM.then(function(val){
                window.setTimeout(
                    function() {
                        mjBig.classList.add('hide');
                        mjBig.classList.remove('show')
                        window.setTimeout(
                            function() {
                                mjBig.remove();
                                mjDiv.remove();
                            }, 1000);

                    }, 800);
            });
        }
    });
}

function playSoundFile(path){

    const AudioContext = window.AudioContext || window.webkitAudioContext;

    let audioContext = null;

    if(audioContext === null){
        audioContext = new AudioContext();
    }
    function playSound(audio) {

        const playSound = audioContext.createBufferSource();
        playSound.buffer = audio;
        playSound.connect(audioContext.destination);
        playSound.start(audioContext.currentTime);
        playSound.onended = function(){
            audioContext = null;
        }
    }
    fetch(path)
        .then(data => data.arrayBuffer())
        .then(arrayBuffer => audioContext.decodeAudioData(arrayBuffer))
        .then(decodedAudio => {
                playSound(decodedAudio);
            }
        );
};
