function renderHtml(urlBasePath){

    const htmlSnippet = `<ul  class="multiple-choice-questionnaire">
    <li>
        <details>
            <summary><span>pkg</span></summary>
            <ol class="multiple-choice">
                <li>hoor pkg<span data-soundid="${urlBasePath}/sounds/peekaagee.ogg" class="speaker material-icons md-24 md-greenish">volume_up</span>
                    <details>
                        <summary><span>nu jij</span></summary>
                        <label>inspreken <button class="record">start</button></label>
                        <button class="stop">stop</button><a class="save">bewaar</a>
                        <article class="sound-clip"></article>
                    </details>
                </li>
                <li>hoor pkg<span data-soundid="${urlBasePath}/sounds/peekaygeeSlow.ogg" class="speaker material-icons md-24 md-greenish">volume_up</span>
                    <details>
                        <summary><span>nu jij</span></summary>
                        <label>inspreken <button class="record">start</button></label>
                        <button class="stop">stop</button><a class="save">bewaar</a>
                        <article class="sound-clip"></article>
                    </details>
                </li>
                <li>hoor pkg<span data-soundid="${urlBasePath}/sounds/peekaygeeFast.ogg" class="speaker material-icons md-24 md-greenish">volume_up</span>
                    <details>
                        <summary><span>nu jij</span></summary>
                        <label>inspreken <button class="record">start</button></label>
                        <button class="stop">stop</button><a class="save">bewaar</a>
                        <article class="sound-clip"></article>
                    </details>
                </li>
                <li>hoor pkg<span data-soundid="${urlBasePath}/sounds/package.ogg" class="speaker material-icons md-24 md-greenish">volume_up</span>
                    <details>
                        <summary><span>nu jij</span></summary>
                        <label>inspreken <button class="record">start</button></label>
                        <button class="stop">stop</button><a class="save">bewaar</a>
                        <article class="sound-clip"></article>
                    </details>
                </li>
            </ol>
        </details>
    </li>
    <li>
        <details>
            <summary><span>unpkg</span></summary>
            <ol class="multiple-choise">
                <li>hoor unpkg<span data-soundid="${urlBasePath}/sounds/u-en-peekagee.ogg" class="speaker material-icons md-24 md-greenish">volume_up</span>
                    <details>
                        <summary><span>nu jij</span></summary>
                        <label>inspreken <button class="record">start</button></label>
                        <button class="stop">stop</button><a class="save">bewaar</a>
                        <article class="sound-clip"></article>
                    </details>
                </li>
                <li>hoor unpkg<span data-soundid="${urlBasePath}/sounds/youanpeekaagee.ogg" class="speaker material-icons md-24 md-greenish">volume_up</span>
                    <details>
                        <summary><span>nu jij</span></summary>
                        <label>inspreken <button class="record">start</button></label>
                        <button class="stop">stop</button><a class="save">bewaar</a>
                        <article class="sound-clip"></article>
                    </details>
                </li>
                <li>hoor unpkg<span data-soundid="${urlBasePath}/sounds/youanpeekaageeSlow.ogg" class="speaker material-icons md-24 md-greenish">volume_up</span>
                    <details>
                        <summary><span>nu jij</span></summary>
                        <label>inspreken <button class="record">start</button></label>
                        <button class="stop">stop</button><a class="save">bewaar</a>
                        <article class="sound-clip"></article>
                    </details>
                </li>
                <li>hoor unpkg<span data-soundid="${urlBasePath}/sounds/unpackage.ogg" class="speaker material-icons md-24 md-greenish">volume_up</span>
                    <details>
                        <summary><span>nu jij</span></summary>
                        <label>inspreken <button class="record">start</button></label>
                        <button class="stop">stop</button><a class="save">bewaar</a>
                        <article class="sound-clip"></article>
                    </details>
                </li>
                <li>hoor unpkg<span data-soundid="${urlBasePath}/sounds/unpackageFast.ogg" class="speaker material-icons md-24 md-greenish">volume_up</span>
                    <details>
                        <summary><span>nu jij</span></summary>
                        <label>inspreken <button class="record">start</button></label>
                        <button class="stop">stop</button><a class="save">bewaar</a>
                        <article class="sound-clip"></article>
                    </details>
                </li>
                <li>hoor unpkg<span data-soundid="${urlBasePath}/sounds/burp.ogg" class="speaker material-icons md-24 md-greenish">volume_up</span>
                    <details>
                        <summary><span>nu jij</span></summary>
                        <label>inspreken <button class="record">start</button></label>
                        <button class="stop">stop</button><a class="save">bewaar</a>
                        <article class="sound-clip"></article>
                    </details>
                </li>
            </ol>
        </details>
    </li>
    <li>
        <details>
            <summary><span>MySQL</span></summary>
            <ol class="multiple-choise">
                <li>hoor MySQL<span data-soundid="${urlBasePath}/sounds/MySQL.ogg" class="speaker material-icons md-24 md-greenish">volume_up</span>
                    <details>
                        <summary><span>nu jij</span></summary>
                        <label>inspreken <button class="record">start</button></label>
                        <button class="stop">stop</button><a class="save">bewaar</a>
                        <article class="sound-clip"></article>
                    </details>
                </li>
                <li>hoor MySQL<span data-soundid="${urlBasePath}/sounds/mysqual.ogg" class="speaker material-icons md-24 md-greenish">volume_up</span>
                    <details>
                        <summary><span>nu jij</span></summary>
                        <label>inspreken <button class="record">start</button></label>
                        <button class="stop">stop</button><a class="save">bewaar</a>
                        <article class="sound-clip"></article>
                    </details>
                </li>
                <li>hoor MySQL<span data-soundid="${urlBasePath}/sounds/mysequel.ogg" class="speaker material-icons md-24 md-greenish">volume_up</span>
                    <details>
                        <summary><span>nu jij</span></summary>
                        <label>inspreken <button class="record">start</button></label>
                        <button class="stop">stop</button><a class="save">bewaar</a>
                        <article class="sound-clip"></article>
                    </details>
                </li>
                <li>hoor MySQL<span data-soundid="${urlBasePath}/sounds/michael-jackson.ogg" class="speaker material-icons md-24 md-greenish">volume_up</span>
                    <details>
                        <summary><span>nu jij</span></summary>
                        <label>inspreken <button class="record">start</button></label>
                        <button class="stop">stop</button><a class="save">bewaar</a>
                        <article class="sound-clip"></article>
                    </details>
                </li>
            </ol>
        </details>
    </li>
    <li>
        <details>
            <summary><span>SQL</span></summary>
            <ol class="multiple-choise">
                <li>hoor SQL<span data-soundid="${urlBasePath}/sounds/eskuel_en.ogg" class="speaker material-icons md-24 md-greenish">volume_up</span>
                    <details>
                        <summary><span>nu jij</span></summary>
                        <label>inspreken <button class="record">start</button></label>
                        <button class="stop">stop</button><a class="save">bewaar</a>
                        <article class="sound-clip"></article>
                    </details>
                </li>
                <li>hoor SQL<span data-soundid="${urlBasePath}/sounds/sequel_en.ogg" class="speaker material-icons md-24 md-greenish">volume_up</span>
                    <details>
                        <summary><span>nu jij</span></summary>
                        <label>inspreken <button class="record">start</button></label>
                        <button class="stop">stop</button><a class="save">bewaar</a>
                        <article class="sound-clip"></article>
                    </details>
                </li>
                <li>hoor SQL<span data-soundid="${urlBasePath}/sounds/eskuel_nl.ogg" class="speaker material-icons md-24 md-greenish">volume_up</span>
                    <details>
                        <summary><span>nu jij</span></summary>
                        <label>inspreken <button class="record">start</button></label>
                        <button class="stop">stop</button><a class="save">bewaar</a>
                        <article class="sound-clip"></article>
                    </details>
                </li>
                <li>hoor SQL<span data-soundid="${urlBasePath}/sounds/michael-jackson.ogg" class="speaker material-icons md-24 md-greenish">volume_up</span>
                    <details>
                        <summary><span>nu jij</span></summary>
                        <label>inspreken <button class="record">start</button></label>
                        <button class="stop">stop</button><a class="save">bewaar</a>
                        <article class="sound-clip"></article>
                    </details>
                </li>
            </ol>
        </details>
    </li>
    <li>
        <details>
            <summary><span>C#</span></summary>
            <ol class="multiple-choise">
                <li>hoor C#<span data-soundid="${urlBasePath}/sounds/siesjarp.ogg" class="speaker material-icons md-24 md-greenish">volume_up</span>
                    <details>
                        <summary><span>nu jij</span></summary>
                        <label>inspreken <button class="record">start</button></label>
                        <button class="stop">stop</button><a class="save">bewaar</a>
                        <article class="sound-clip"></article>
                    </details>
                </li>
                <li>hoor C#<span data-soundid="${urlBasePath}/sounds/sieeeeesjaaaarp.ogg" class="speaker material-icons md-24 md-greenish">volume_up</span>
                    <details>
                        <summary><span>nu jij</span></summary>
                        <label>inspreken <button class="record">start</button></label>
                        <button class="stop">stop</button><a class="save">bewaar</a>
                        <article class="sound-clip"></article>
                    </details>
                </li>
                <li>hoor C#<span data-soundid="${urlBasePath}/sounds/cis.ogg" class="speaker material-icons md-24 md-greenish">volume_up</span>
                    <details>
                        <summary><span>nu jij</span></summary>
                        <label>inspreken <button class="record">start</button></label>
                        <button class="stop">stop</button><a class="save">bewaar</a>
                        <article class="sound-clip"></article>
                    </details>
                </li>
                <li>hoor C#<span data-soundid="${urlBasePath}/sounds/cool.ogg" class="speaker material-icons md-24 md-greenish">volume_up</span>
                    <details>
                        <summary><span>nu jij</span></summary>
                        <label>inspreken <button class="record">start</button></label>
                        <button class="stop">stop</button><a class="save">bewaar</a>
                        <article class="sound-clip"></article>
                    </details>
                </li>
                <li>hoor C#<span data-soundid="${urlBasePath}/sounds/kool.ogg" class="speaker material-icons md-24 md-greenish">volume_up</span>
                    <details>
                        <summary><span>nu jij</span></summary>
                        <label>inspreken <button class="record">start</button></label>
                        <button class="stop">stop</button><a class="save">bewaar</a>
                        <article class="sound-clip"></article>
                    </details>
                </li>
                <li>hoor C#<span data-soundid="${urlBasePath}/sounds/seeee_hekje.ogg" class="speaker material-icons md-24 md-greenish">volume_up</span>
                    <details>
                        <summary><span>nu jij</span></summary>
                        <label>inspreken <button class="record">start</button></label>
                        <button class="stop">stop</button><a class="save">bewaar</a>
                        <article class="sound-clip"></article>
                    </details>
                </li>
                <li>hoor C#<span data-soundid="${urlBasePath}/sounds/seehash.ogg" class="speaker material-icons md-24 md-greenish">volume_up</span>
                    <details>
                        <summary><span>nu jij</span></summary>
                        <label>inspreken <button class="record">start</button></label>
                        <button class="stop">stop</button><a class="save">bewaar</a>
                        <article class="sound-clip"></article>
                    </details>
                </li>
                <li>hoor C#<span data-soundid="${urlBasePath}/sounds/seekruis.ogg" class="speaker material-icons md-24 md-greenish">volume_up</span>
                    <details>
                        <summary><span>nu jij</span></summary>
                        <label>inspreken <button class="record">start</button></label>
                        <button class="stop">stop</button><a class="save">bewaar</a>
                        <article class="sound-clip"></article>
                    </details>
                </li>
                <li>hoor C#<span data-soundid="${urlBasePath}/sounds/sie_ou_ou_ell.ogg" class="speaker material-icons md-24 md-greenish">volume_up</span>
                    <details>
                        <summary><span>nu jij</span></summary>
                        <label>inspreken <button class="record">start</button></label>
                        <button class="stop">stop</button><a class="save">bewaar</a>
                        <article class="sound-clip"></article>
                    </details>
                </li>
                <li>hoor C#<span data-soundid="${urlBasePath}/sounds/zee_o_o_el.ogg" class="speaker material-icons md-24 md-greenish">volume_up</span>
                    <details>
                        <summary><span>nu jij</span></summary>
                        <label>inspreken <button class="record">start</button></label>
                        <button class="stop">stop</button><a class="save">bewaar</a>
                        <article class="sound-clip"></article>
                    </details>
                </li>
            </ol>
        </details>
    </li>
    <li>
        <details>
            <summary><span>#!</span></summary>
            <ol class="multiple-choise">
                <li>hoor #!<span data-soundid="${urlBasePath}/sounds/shebang.ogg" class="speaker material-icons md-24 md-greenish">volume_up</span>
                    <details>
                        <summary><span>nu jij</span></summary>
                        <label>inspreken <button class="record">start</button></label>
                        <button class="stop">stop</button><a class="save">bewaar</a>
                        <article class="sound-clip"></article>
                    </details>
                </li>
                <li>hoor #!<span data-soundid="${urlBasePath}/sounds/shabang.ogg" class="speaker material-icons md-24 md-greenish">volume_up</span>
                    <details>
                        <summary><span>nu jij</span></summary>
                        <label>inspreken <button class="record">start</button></label>
                        <button class="stop">stop</button><a class="save">bewaar</a>
                        <article class="sound-clip"></article>
                    </details>
                </li>
                <li>hoor #!<span data-soundid="${urlBasePath}/sounds/poundbang.ogg" class="speaker material-icons md-24 md-greenish">volume_up</span>
                    <details>
                        <summary><span>nu jij</span></summary>
                        <label>inspreken <button class="record">start</button></label>
                        <button class="stop">stop</button><a class="save">bewaar</a>
                        <article class="sound-clip"></article>
                    </details>
                </li>
                <li>hoor #!<span data-soundid="${urlBasePath}/sounds/hashbang.ogg" class="speaker material-icons md-24 md-greenish">volume_up</span>
                    <details>
                        <summary><span>nu jij</span></summary>
                        <label>inspreken <button class="record">start</button></label>
                        <button class="stop">stop</button><a class="save">bewaar</a>
                        <article class="sound-clip"></article>
                    </details>
                </li>
                <li>hoor #!<span data-soundid="${urlBasePath}/sounds/hashexclaim.ogg" class="speaker material-icons md-24 md-greenish">volume_up</span>
                    <details>
                        <summary><span>nu jij</span></summary>
                        <label>inspreken <button class="record">start</button></label>
                        <button class="stop">stop</button><a class="save">bewaar</a>
                        <article class="sound-clip"></article>
                    </details>
                </li>
                <li>hoor #!<span data-soundid="${urlBasePath}/sounds/hashpling.ogg" class="speaker material-icons md-24 md-greenish">volume_up</span>
                    <details>
                        <summary><span>nu jij</span></summary>
                        <label>inspreken <button class="record">start</button></label>
                        <button class="stop">stop</button><a class="save">bewaar</a>
                        <article class="sound-clip"></article>
                    </details>
                </li>
            </ol>
        </details>

    </li>
</ul>
`;
    return htmlSnippet;
}
export { renderHtml }