import { renderHtml as renderHtml } from './htmlsnippet.js';

class ListenSpeak extends HTMLElement {
    constructor(){
        super();
        // window.onbeforeunload = function(){
        //     return 'Are you sure you want to leave?';
        // };
        let urlBasePath = this.getAttribute('data-url-basepath');
        // console.log(urlBasePath)

        navigator.getUserMedia = ( navigator.getUserMedia ||
            navigator.webkitGetUserMedia ||
            navigator.mozGetUserMedia ||
            navigator.msGetUserMedia);

        this.audioContext;
        this.mediaStream = null;
        this.mediaRecorder = null;
        this.activeBlocks = [];
        this.thisTimeOut;

        this.shadow = this.attachShadow({mode: 'open'});
        let innerULShadow = renderHtml(urlBasePath);


        this.shadow.innerHTML = `
<style type="text/css">
  @import url("${urlBasePath}/webfonts/material-icons.css");
  @import url("${urlBasePath}/assets/styles.css");
  @import url("${urlBasePath}/assets/shadow.css");
}
</style>
<slot name="hear-speak-title">
    <!--The <img> from the light DOM gets rendered here!-->
</slot>
${innerULShadow}
        
`;
        this.initiate(this.shadow);
    }// end of constructor


    /**
     * Stops the current sound recording.
     * This function can be called by clicking the appropriate stop button, of by the timeout of the recording.
     *
     * @param {object} target - DOM element - Can be a record button whose action is stopped by a timeout, or a stop button
     */
    stopRecording(target){
        //re-enable record buttons
        this.activeBlocks.forEach(function(value){
            let recordButtons = value.querySelectorAll("button.record");
            // stopped,so enable all record buttons
            recordButtons.forEach(function(element){
                element.removeAttribute("disabled");
            });
        });

        let thisRecordButton;
        if (target.tagName === "BUTTON"){
            if (target.classList.contains("record")){
                thisRecordButton = target;
            }else if(target.classList.contains("stop")){
                clearTimeout(this.thisTimeOut);
                thisRecordButton = target.previousElementSibling.querySelector("button.record");
            }else{
                console.log("unkown error");
            }
        }
        thisRecordButton.style.background = "";
        thisRecordButton.style.color = "";
        this.mediaRecorder.stop();
        // console.log(this.mediaStream.getTracks()[0]);
        // console.log(this.mediaStream.getTracks());

        //To end the microphone stream and warning icon
        //TODO: handle user disallowing microphone
        //TODO: put this ending under button?
        //TODO: put this in page unload event (android blocks microphone for other processes, at least on my phone)
        this.mediaStream.getTracks().forEach(function (track){
            track.stop();

        });
        //https://developer.mozilla.org/en-US/docs/Web/API/MediaStreamTrack/stop
        this.mediaStream = null;
        this.mediaRecorder = null;
        // daarmee verdwijnt wel het microfoontje, maar daarna is het een probleem dat er geen track meer is... wat dat mag betekenen
        // mi9sschien https://developers.google.com/web/updates/2015/07/mediastream-deprecations?hl=en#stop-ended-and-active
        // https://stackoverflow.com/questions/11642926/stop-close-webcam-which-is-opened-by-navigator-getusermedia

    }

    /**
     * Prepare and start recording sound.
     *
     * @param {object} target - DOM element - the record button clicked
     */
    startRecording (target) {
        let thisHere = this; //needed for callbacks here

        // console.log(target.parentElement.nextElementSibling);

        this.activeBlocks.forEach(function(value){
            let recordButtons = value.querySelectorAll("button.record");
            // while recording, disable all record buttons
            recordButtons.forEach(function(element){
                element.setAttribute("disabled","disabled");
            });
        });

        target.style.background = "red";
        target.style.color = "black";

        //start recording
        this.mediaRecorder.start();

        // set a timeout to stop recording
        this.thisTimeOut = setTimeout(function () {
            thisHere.stopRecording(target);
        },3000);

        // ondataavailable is fired when recording is stopped
        this.mediaRecorder.ondataavailable = function(e){

            let downloadAnker = target.parentElement.nextElementSibling.nextElementSibling;


            let thisLi = target.parentElement.parentElement.parentElement;
            let pathstring = thisLi.querySelector("span[data-soundid]").getAttribute("data-soundid");

            // cleanup string
            // remove anything before the first "/" (that is not a "/")
            // remove everything that starts with a "/" and ends with a "/"
            // remove ".ogg" at the end of the string
            let testreg = /((^[^/]+(?=\/))|(\/.*\/)|(\.ogg$))/g;
            let filetitle = pathstring.replace(testreg,"");

            let clipName = "jouw " + filetitle;
            let clipContainer = document.createElement('article');
            let clipLabel = document.createElement('p');
            let audio = document.createElement('audio');

            clipContainer.classList.add('clip');
            audio.setAttribute('controls', '');
            clipLabel.innerHTML = clipName;
            clipContainer.appendChild(clipLabel);
            clipContainer.appendChild(audio);

            let thisSoundClip = target.parentElement.parentElement.querySelector("article.sound-clip");
            thisSoundClip.innerHTML = "";
            thisSoundClip.appendChild(clipContainer);

            let datatype = e.data.type;
            let subdatatypestring = datatype.replace(/^audio\//,'');
            // on chrome e.data.type is :audio/webm;codecs=opus" so extra cleanup:
            // remove everything after (and including) ";"
            let extension = subdatatypestring.replace( /;.*$/,"");

            downloadAnker.download = Date.now() + "_mijn_" + filetitle + "." + extension;
            let blobURL = URL.createObjectURL(e.data);
            audio.src = blobURL;
            downloadAnker.href = blobURL;
            downloadAnker.type = e.data.type;
            //https://hacks.mozilla.org/2014/06/easy-audio-capture-with-the-mediarecorder-api/ !!
        }
    }


    /**
     *
     * @param {object} target - DOM element - The record button clicked
     */
    createMediarecorder(target){
        let thisHere = this; //needed for callbacks here
        // console.log(navigator.mediaDevices.getUserMedia);
        if (navigator.mediaDevices.getUserMedia) {
            if (this.mediaStream === null){
                //https://developer.mozilla.org/en-US/docs/Web/API/MediaDevices/getUserMedia  //!! werkt ook op chrome on android
                let constraints = { audio: true };
                navigator.mediaDevices.getUserMedia(constraints)
                .then(function (mediaStream) {

                    thisHere.mediaStream = mediaStream;
                    // thisHere.mediaStream.stop() ???

                    // now create the recorder
                    thisHere.mediaRecorder = new MediaRecorder(thisHere.mediaStream);
                    thisHere.startRecording(target);

                }).catch(function(err) { console.log(err.name + ": " + err.message); }); // always check for errors at the end.
            }else{
                alert("sorry, dit gaat niet werken.");
            }

            // navigator.mediaDevices.getUserMedia(constraints)
            // .then(function(mediaStream) {
            //     var video = document.querySelector('video');
            //     video.srcObject = mediaStream;
            //     video.onloadedmetadata = function(e) {
            //         video.play();
            //     };
            // })
            // .catch(function(err) { console.log(err.name + ": " + err.message); }); // always check for errors at the end.

        }else {
            console.log('getUserMedia not supported on your browser!');
            alert("sorry, dit gaat niet werken in uw browser");
        }
        ///////////////////////////////////

        // if (navigator.getUserMedia) {
        // // if (navigator.mediaDevices.getUserMedia) {
        //     // console.log('getUserMedia supported.');
        //     if (this.mediaStream === undefined){
        //         navigator.getUserMedia (
        //             // constraints - only audio needed for this app
        //             {
        //                 audio: true
        //             },
        //             // Success callback
        //             function(stream) {
        //                 thisHere.mediaStream = stream;
        //
        //                 // now create the recorder
        //                 thisHere.mediaRecorder = new MediaRecorder(thisHere.mediaStream);
        //                 thisHere.startRecording(target);
        //             },
        //
        //             // Error callback
        //             function(err) {
        //                 console.log('The following gUM error occured: ' + err);
        //             }
        //         );
        //     }
        // }else {
        //     console.log('getUserMedia not supported on your browser!');
        //     alert("sorry, dit gaat niet werken in uw browser");
        // }
    }

    /**
     *
     * @param {object} thisSpeaker - DOM element - The current speaker icon clicked on
     * @param {string} oldColor - The color of the speaker icon before the click
     * @param {object} audio - decoded audio - The audio data to play
     */
    playSound(thisSpeaker, oldColor, audio) {

        const playSound = this.audioContext.createBufferSource();
        playSound.buffer = audio;
        playSound.connect(this.audioContext.destination);
        playSound.start(this.audioContext.currentTime);
        playSound.onended = function(){
            thisSpeaker.style.color = oldColor;
        }
    }


    /**
     *
     * @param {string} soundFilePath
     * @param {object} thisSpeaker - DOM element
     * @param {string} oldColor
     */
    getSoundFromFile(soundFilePath, thisSpeaker, oldColor){

        let thisHere = this; //needed for callbacks here

        if(this.audioContext === undefined){
            this.audioContext = new AudioContext();
        }

        fetch(soundFilePath)
            .then(data => data.arrayBuffer())
            .then(arrayBuffer => this.audioContext.decodeAudioData(arrayBuffer))
            .then(decodedAudio => {
                    // audio = decodedAudio;
                    thisHere.playSound(thisSpeaker,oldColor,decodedAudio);
                }
            );
    }

    /**
     *
     * @param {object} targetDOMpart - DOM element - The DOM element receiving initialisation
     */
    initiate(targetDOMpart){
        let thisHere = this; //needed for callbacks here
        this.activeBlocks.push(targetDOMpart);
        targetDOMpart.addEventListener("click", function(event){

                if(event.target.classList.contains("speaker")){
                    let oldColor = event.target.style.color;
                    let thisSpeaker = event.target;
                    event.target.style.color = "red";

                    let soundFilePath = event.target.attributes.getNamedItem("data-soundid").value;
                    thisHere.getSoundFromFile(soundFilePath,thisSpeaker,oldColor);
                }
                if (event.target.classList.contains("record")) {
                    if(thisHere.mediaRecorder === undefined || thisHere.mediaRecorder === null){
                        thisHere.createMediarecorder(event.target);
                    }else{
                        thisHere.startRecording(event.target);
                    }
                }
                if(event.target.classList.contains("stop")){
                    thisHere.stopRecording(event.target);
                }
            }
        );

    }
}
export { ListenSpeak }