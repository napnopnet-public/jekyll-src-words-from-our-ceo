# jekyll-src-words-from-our-ceo  


This is 'before build' code for jekyll (static site generator), for a blog on gitlab pages: [napnopnet.gitlab.io/words-from-our-ceo](https://napnopnet.gitlab.io/words-from-our-ceo/). 


The file 'example_gitlab-ci_yml' contains content for .gitlab-ci.yml for use with gitlab pages. Place it in the _site directory (.gitignored here) and rename it to '.gitlab-ci.yml'.  


Jekyll was installed on windows 10, running ruby activated with uru. Jekyll commands are run with Windows PowerShell
