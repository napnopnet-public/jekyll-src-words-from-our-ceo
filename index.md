---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults
layout: home
lang: nl
---
 {% comment %} https://stackoverflow.com/questions/43120788/jekyll-display-all-posts-in-the-same-page {% endcomment %}
{% for post in site.posts %}
  {% if  post.url  contains ".backup"  %}
  {% else %}
  
  {% if post.idhash == undifined %}
  <article>
  {% else %}
  <article id="{{  post.idhash }}">
  {% endif %}
  
    <h2>
      <a href="{{ site.baseurl }}{{ post.url }}">
        {{ post.title }}
      </a>
    </h2>
	{% comment %}{{ post.post_id }}{% endcomment %}
	<p />
   {% comment %}<time datetime="{{ post.date | date: "%Y-%m-%d" }}">{{ post.date | date_to_long_string }}</time> {% endcomment %}
   {% comment %}https://ruby-doc.org/stdlib-2.6.2/libdoc/time/rdoc/Time.html#method-c-parse{% endcomment %}
   {% comment %}https://github.com/nelsonsar/jekyll-i18n-filter{% endcomment %}
   {% comment %}https://raw.githubusercontent.com/nelsonsar/jekyll-i18n-filter/master/i18n_filter.rb{% endcomment %}
   {% comment %}https://raw.githubusercontent.com/svenfuchs/rails-i18n/master/rails/locale/nl.yml{% endcomment %}
   
   <time datetime="{{ post.date | localize: "%A %e %B %Y %e/%m/%Y"  }}">{{ post.date | localize: "%A %e %B %Y"  }}</time>
    {{ post.content }}
  </article>
  {% endif %}
{% endfor %}

{% comment %} D:/rubies/Ruby25-x64/lib/ruby/gems/2.5.0/gems/minima-2.5.0 {% endcomment %}

