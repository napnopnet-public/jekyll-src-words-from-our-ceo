---
layout: post
lang: nl
title:  "\n\nGUI for folder synchronization for windows (using Robocopy)"
date:   2020-10-09 12:03:33 +0100
categories: coding
# create a hash for this post for instance with $ md5sum <<<$(date)  on linux, or cygwin and put it in di attribute of the span element in post_id
post_id: |
    <span class="post_comment" data-postid="8aa12c83d953d917f1e8bd00ac0590a8">
    </span>
idhash: 8aa12c83d953d917f1e8bd00ac0590a8
---
<p></p>

# Robocopy
Windows desktop application for continuous synchronization of  folders (using Robocopy, just for now).
It uses a repeated Robocopy command in a loop. 
 

https://drive.google.com/file/d/1K14aYq_Jktha22d3UwhnSFNyqEcxabc1/view?usp=sharing

[![icon Keep-Syncing]({{ site.baseurl }}/images/keep-syncingIco.png "Download Keep-Syncing")]( https://drive.google.com/uc?export=download&id=1K14aYq_Jktha22d3UwhnSFNyqEcxabc1) 
[Download Keep-Syncing](https://drive.google.com/uc?export=download&id=1K14aYq_Jktha22d3UwhnSFNyqEcxabc1) 


hash in powershell:  
PS C:\downloads> get-filehash KeepSyncing.exe

Algorithm       Hash                                                           
---------       ----                                                            
SHA256          C2F93166CD0CC1B32DD42984A074CDB84D7B4391F053EFD6DCD0C90E0266FF33  

and:  

PS C:\downloads>  get-filehash KeepSyncing.zip

Algorithm       Hash                                                             
---------       ----                                                            
SHA256          002D433CB4CA066EC31C2D0942E1082CF2DF24F3534BEB842DE45670F2381D67      



<details>
<summary><i>License</i></summary>
<pre>
MIT License

Copyright (c) 2020 (napnop.net) napnopnet-public / C#

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

</pre>
</details>

Download,  
unzip,  
start keep-syncing.exe, and trust it,   
Choose a "mode" ("Mirror", "Backup" or "Custom"),  
choose a "source folder" and a "destination folder"  
choose no or more "excluded folders",  
choose no or more "excluded files",  
enter no or more "excluded extensions",  
when in "custom mode", enter robocopy options, 
hit "test robocopy string",  
if ok: hit "start robocopy loop",  
Ready?: hit "stop"  
<script type="text/javascript" src="{{ site.baseurl }}/assets/mjPopup.js" />

<script type="text/javascript">
  initiateMjPict("{{ site.baseurl }}");
</script>
<section style="font-style:italic;padding-left:20px;color:gray ">
</section>
              
<details>
<summary><i>Zie</i></summary>
  <ul>
    
    <li><a target="_blank" href="https://nl.wikipedia.org/wiki/Robocopy">Robocopy wikipedia NL</a></li>
    <li><a target="_blank" href="https://en.wikipedia.org/wiki/Robocopy">Robocopy wikipedia EN</a></li>
    
  </ul>
</details> 
<br/>
<p />
<br /><br />
{{ page.post_id }}