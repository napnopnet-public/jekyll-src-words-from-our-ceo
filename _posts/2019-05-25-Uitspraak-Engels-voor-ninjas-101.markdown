---
layout: post
lang: nl
title:  "\n\nUitspraak Engels voor ninja's 101"
date:   2019-05-25 10:03:33 +0200
categories: coding
# create a hash for this post for instance with $ md5sum <<<$(date)  on linux, or cygwin and put it in di attribute of the span element in post_id
post_id: |
    <span class="post_comment" data-postid="de8f986bbbf3f90b732c82b631761136">
    </span>
idhash: de8f986bbbf3f90b732c82b631761136
---
<p></p>

# M. J.
Het begon met de term "unpkg". Ik was er net achter gekomen dat het uploaden van een (javascript-) nodejs-package (pakket) naar https://www.npmjs.com, niet alleen een distributiekanaal voor je package gaf op npmjs.com, maar ook een "content delivery network" (CDN of cdn) via https://unpkg.com/. Tussen mijn aangename verrassing door vroeg ik me wel af wat "unpkg" nou eigenlijk betekent of waarnaar het verwijst. Google-en leverde me niets op, maar de vraag bleef in mijn subbewuste brein aanwezig.  
Verdergaand met me interesseren voor unpkg.com als cdn trof ik een <a target="_blank" href="https://www.youtube.com/watch?v=2rhkgB8Cohc">video aan met een praatje</a> van (jawel) 
<a class="Michael_J" href="">
    <span data-bigpict-src="{{ site.baseurl }}/images/Michael_Jackson_unpkg.jpg" data-sound-src="{{ site.baseurl }}/sounds/michael-jackson.ogg">Michael Jackson!</span>
</a>
<script type="text/javascript" src="{{ site.baseurl }}/assets/mjPopup.js" />

<script type="text/javascript">
  initiateMjPict("{{ site.baseurl }}");
</script>
<section style="font-style:italic;padding-left:20px;color:gray ">[--- Hoe het brein omgaat met tweede talen en culturele invasie/infestering: als ik 
<a class="Michael_J" href="">
<span data-bigpict-src="{{ site.baseurl }}/images/Michael_Jackson_1984.jpg" data-sound-src="{{ site.baseurl }}/sounds/michael-jackson.ogg">"Michael Jackson!"</span>
</a>
 lees of hoor, denk ik gelijk en uitsluitend aan onze grote kindervriend en muziekwonderkoning. Geen grein in mijn bewustzijn staat open voor de gedachte dat de naam 
<a class="Michael_J" href="">
<span data-bigpict-src="{{ site.baseurl }}/images/Michael_jackson_beer.jpg" data-sound-src="{{ site.baseurl }}/sounds/michael-jackson.ogg">"Michael Jackson!"</span>
</a> in de <a target="_blank" href="https://www.facebook.com/public/Michael-Jackson"> 
VS waarschijnlijk wel vaker voorkomt</a>, sterker nog, een soort "Peter de Vries" -naam (zonder R) zou kunnen zijn ---]</section>
                                                                                                                                                                                                                                                                                  
<details>
<summary><i>Zie</i></summary>
  <ul>
    <li><a target="_blank" href="https://nl.wikipedia.org/wiki/Michael_Jackson_(bierkenner)">Bierexpert M. Jackson</a></li>
    <li><a target="_blank" href="https://www.facebook.com/public/Michael-Jackson">Michael Jacksons bij facebook</a></li>
    <li><a target="_blank" href="https://commons.wikimedia.org/wiki/File:Michael_jackson_beer.jpg">foto-info bierexpert</a></li>
    <li><a target="_blank" href="https://commons.wikimedia.org/wiki/File:Michael_Jackson_1984.jpg">foto-info muziekwonderkoning</a></li>
  </ul>
</details> 

<br/>
# Maiziekwel
Bij het kijken naar <a target="_blank" href="https://www.youtube.com/watch?v=2rhkgB8Cohc">de video</a> had ik, naast een antwoord op mijn sluimerende vraag,  een beetje de ervaring die ik al eens eerder had gehad tijdens een praatje van iemand over MySql.  

Ik kon dit praatje aanvankelijk in het geheel niet volgen omdat de iemand het had over "???", zoiets als "maiziekwel" of zo, en het "woord" MySql niet gebruikte (klinkt als "mai es kuhjoe el" of "mai es kjuu el"), en het "woord" SQL geloof ik ook verving met "ziekwel". Toen mijn brein deze woordvervanging had geconstateerd en redelijk snel kon toepassen, werd het praatje wat begrijpelijker.  

Enig literatuuronderzoek later leerde me dat dit fenomeen wel is gebaseerd op een <a target="_blank" href="https://en.wikipedia.org/wiki/SQL#History">bepaalde geschiedenis</a> (iets met beschermde (handels)namen). Maar hoe kon ik dat nou weten? En waarom zou ik dat moeten weten? Was het wel de bedoeling dat ik het praatje begreep? Was het niet een middel voor genoemd iemand om te laten zien hoe ingewijd hij wel niet was (en ik dus niet)? 

Was het een geval van kliekpraat, jargon dat als kliekbevestiging, kliekherkenning, kliektoegangsbelemmering dient? 
<section style="font-style:italic;padding-left:20px;color:gray ">[--- kliek: gesloten groep personen die het applaus ontvangen van, of verwachten van, buitenkliekers ≈ in-crowd.
<details>
<summary><i>Zie</i></summary>
  <ul>
    <li><a href="http://gtb.ivdnt.org/iWDB/search?actie=article&wdb=WNT&id=M033721">WNT: KLIEK</a></li>
    <li><a href="https://www.etymonline.com/word/clique">etymonline.com: clique (n.)</a></li>
    <li><a href="https://dougbennettblog.wordpress.com/2014/10/03/meanings-clique/">https://dougbennettblog.wordpress.com/2014/10/03/meanings-clique/</a></li>
  </ul>
</details> 
---]
</section>
  
<p />
# Ninja of Stumper?
Subtiel en problematisch  bij dit alles is dat ik genoemd iemand dan weer een beetje als stumper beleefde.  

Elk voordeel heeft z'n nadeel. Kenmerken uitdragen van een kliek kan onverwachte  nadelen hebben die je niet zonder meer kunt waarnemen. Voor wie/wat spreek/schrijf/kleed/handel je of gedraag je je, met welk doel? Is de kliek waar je denkt bij te willen horen, niet precies het kenmerk dat verhindert te kunnen behoren tot de echt belangrijke, nog niet voor jou zichtbare, andere kliek? Doet dat kenmerk de afstand tussen jezelf en je publiek niet vergroten in plaats van een gevoel van gemeenschappelijkheid te veroorzaken?  

Neem nou het begrip code-ninja/coding ninja: voor de ene groep is het een groots eerbetoon als  iemand wordt uitgemaakt voor coding ninja, voor de andere groep is het een kenmerk van <a target="_blank" href="https://softwareengineering.stackexchange.com/questions/23121/why-are-good-programmers-referred-to-as-ninjas#answer-23127">pretentieuze net-niet-helemalen</a>.
 
Vanwege de complexiteit van deze sociale omgangsnormen en bijbehorende keuzestress hieronder een kleine veilige, oordeelloze testruimte voor een aantal andere voorbeelden van kliekbepalende uitspraakwijzen van sociaal gevaarlijke termen. 

<article>
    <listen-speak  data-url-basepath="{{ site.baseurl }}">
        <style type="text/css">
            @import url("{{ site.baseurl }}/webfonts/material-icons.css"); /* I could not find a way to get @font-face in shadow dom working*/
        </style>
        <h2 slot="hear-speak-title">Luister, huiver en spreek (het zelf beter uit)</h2>
        listen and speak component
    </listen-speak>
</article>
<script type="module">
    import { initiateListenSpeak as initiateListenSpeak } from '{{ site.baseurl }}/assets/initiate-listen-speak.js';
    initiateListenSpeak("{{ site.baseurl }}");
</script>

<br /><br />
{{ page.post_id }}