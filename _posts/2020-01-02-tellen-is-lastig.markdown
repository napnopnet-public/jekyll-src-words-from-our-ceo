---
layout: post
lang: nl
title:  "\n\nTellen is lastig."
date:   2020-01-02 13:03:45 +0200
categories: mens-en-ambacht
# create a hash for this post for instance with $ md5sum <<<$(date)  on linux, or cygwin and put it in di attribute of the span element in post_id
post_id: |
    <span class="post_comment" data-postid="0a9b2e279d61566d8f7be0f288d0a4f3">
    </span>
    <details open>
    <summary>Commentaar</summary>
    
    <div class="item-comments" id="comments-0a9b2e279d61566d8f7be0f288d0a4f3">
    <ol>
      <!-- Create an instance of the todo-item component -->
      <commentor-login-button
       v-bind:button-status="statusList[1]"
       v-bind:key="statusList[1].id"
        
        
        
      ></commentor-login-button>
    </ol>
    
    
    <input v-model="preview" /><p>{{ preview }}</p></div>
    </details>
---
<p></p> 
<section style="font-style:italic;padding-left:20px;color:gray ">[--- Trefwoorden:  tiental, jaar, tiental van jaren, decennium, millenniumvergissing
 ---]</section>
 <br />
 
 
[![tellen is lastig: een tiental]({{ site.baseurl }}/images/tellenRomeins.jpg "tellen is lastig: een tiental")]({{ site.baseurl }}/images/tellenRomeins.jpg)


Hoeveel zitten er in een tiental?  
~~~~~
10
~~~~~
Wat is de eerste van een tiental?  
~~~~~
1
~~~~~
Wat is de laatste in een tiental?  
~~~~~
10
~~~~~

Dat was het 1e tiental.  

Hoeveel zitten er in het tweede tiental?  
~~~~~
10
~~~~~
Wat is de eerste van het tweede tiental?  
~~~~~
11
~~~~~
Wat is de laatste van het tweede tiental?  
~~~~~
20
~~~~~

etc. etc.  

Hoeveel zitten er in het 202e tiental?  
~~~~~
10
~~~~~
Wat is de eerste van het 202e tiental?  
~~~~~
2011
~~~~~
Wat is de laatste van het 202e tiental?  
~~~~~
2020
~~~~~

Hoeveel zitten er in het 203e tiental?  
~~~~~
10
~~~~~
Wat is de eerste van het 203e tiental?  
~~~~~
2021
~~~~~
Wat is de laatste van het 203e tiental?  
~~~~~
2030
~~~~~

Wanneer begint het 203e tiental?  
~~~~~
Precies tussen 2020 en 2021
~~~~~

Dus als het om jaren gaat: tussen 2020 en 2021, dus op het moment dat 2020 eindigt!! Dat duurt nog net iets minder dan een jaar.  
Overigens is "decennium" is een ander woord voor "een tiental van jaren".
<p style="font-style:italic;font-size:0.7em">
*(Een troost voor mensen die het gevoel hebben dat er iets raars aan de hand is: er is ook een "andere" wijze van kijken naar decennia/decenniums, volgens wikipedia in het engels "the cardinal method", waarbij het eerste decennium loopt van 10 t/m 19 (serieus? Wanneer begint dan de nieuwe eeuw, of het nieuwe millennium? 2010? Of vallen de eerste 99 jaar van/voorafgaand aan de eerste eeuw ook weg, en de eerste 999 van/voorafgaand aan het eerste millennium? Is Christus dus al meer dan 3000 jaar geleden geboren? ). Mij lijkt dat niet meer dan een goed klinkende term voor "onzinnige decenniumrekening", "geïnstitutionaliseerde vergissing/verwarring" of iets uitgedruk in een onbeschaafdere term.)*
</p>



<br />
<div class="overflower" style="overflow:auto" id="image-result">
</div>

<details>
<summary><i>Zie</i></summary>
  <ul>
    <li><a target="_blank" href="https://en.wikipedia.org/wiki/Decade">Decade EN</a></li>
    <li><a target="_blank" href="https://nl.wikipedia.org/wiki/Decade_(tijd)">Decade NL</a></li>
    <li><a target="_blank" href="https://nl.wikipedia.org/wiki/Decennium">Decennium</a></li>
    <li><a target="_blank" href="https://nl.wikipedia.org/wiki/Millenniumkwestie">Millenniumvergissing</a></li>
    <li><a target="_blank" href="https://wikikids.nl/Millenniumvergissing">Millenniumvergissing, voor kinderen</a></li>
  </ul>
</details>

<p></p> 
{{ page.post_id }}