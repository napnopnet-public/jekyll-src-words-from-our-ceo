---
layout: post
lang: nl
title:  "\n\nkoalaman's ShellCheck, op Windows 10: linter (ww) je shellscripts!"
date:   2019-03-06 13:03:33 +0200
categories: coding
# create a hash for this post for instance with $ md5sum <<<$(date)  on linux, or cygwin and put it in di attribute of the span element in post_id
post_id: |
    <span class="post_comment" data-postid="29bd629b522be7d50f59e79adbdf0e2d">
    </span>
---
<p></p> 
In het algemeen wordt bij softwareontwikkeling met "lint" of "linter" een applicatie bedoeld die softwarebroncode controleert op juiste syntaxis en codestijl.  
"Lint" is afgeleid van [een Unix-hulpprogrammaatje met de naam "lint" ("pluis")](https://en.wikipedia.org/wiki/Lint_(software)). Anderzijds, een "linter", zoals hier en daar dergelijke applicaties worden genoemd, is ook ["a machine for removing the short fibers from cotton seeds after ginning"](https://en.wikipedia.org/wiki/Linter_(disambiguation)) (ontkorrelmachine of egreneermachine). ("Ginning" is dan weer afgeleid van het door de Engelsen verhaspelde Nederlandse woord jenever, "gin", dat tevens de betekenis "sterk", "hard" of "ruw" heeft gekregen, maar dat terzijde:-).)  

[![Plaatje ontkorrelmachine]({{ site.baseurl }}/images/ontkorrelmachine.gif "plaatje geleend")]( https://www.dbnl.org/tekst/beug001kato01_01/beug001kato01_01_0004.php)  
*Een geleend plaatje van een linter: klik of plaatje voor bron*


Tijdens het fröbelen met gitlab's "continuous integration"-systeem en het voortraject onder de hoede van mijn locale git, was ik bezig met een bashscript dat via een git-alias geactiveert wordt en vervolgens een "php -l" (php-syntaxiscontrole) commando uitvoert op files die nog niet in het git-depository zijn toegevoegd. Wie beschrijft mijn verbazing dat ik mezelf afvroeg: "is er een lintding voor bashscripts?". Hoe dan ook, afgeleid zijnde ging ik aan het google-en en concludeerde dat [koalaman's shellcheck](https://github.com/koalaman/shellcheck) de eerst en meest genoemde kandidaat zou moeten zijn.  


Na enig onbegrip van mijn kant, is dit ongeveer wat ik heb gedaan: 

* [Haskell voor windows](https://www.haskell.org/platform/windows.html) gedownload en geinstalleerd. 
    - daarna was het "cabal" - commando beschikbaar.  
* Aanwijzingen gevolgd onder kopje ["Compiling ShellCheck"](https://github.com/koalaman/shellcheck/blob/master/README.md#compiling-shellcheck) van de [README.md](https://github.com/koalaman/shellcheck/blob/master/README.md) van de [shellcheck-repository](https://github.com/koalaman/shellcheck).  
    - "git clone" uitgevoerd van de [repository voor shellcheck](https://github.com/koalaman/shellcheck) ergens locaal en de gekloonde directory in geceedeed.
    - "cabal install" - commando uitgevoerd.  
	    - daarna was shellcheck.exe neergezet in  C:\Users\Gebruiker&#x200b;\AppData\Roaming&#x200b;\cabal\bin\shellcheck.exe en  
		- de directory was in het path aanwezig. 


![enig commentaar op niet al te beste bash-code]({{ site.baseurl }}/images/badBash.jpg "enig commentaar op niet al te beste bash-code")  
*Een debacle op shellscriptingniveau*  


Conclusie: ik moet nodig leren bashscripts te schrijven. 

<p></p> 
{{ page.post_id }}