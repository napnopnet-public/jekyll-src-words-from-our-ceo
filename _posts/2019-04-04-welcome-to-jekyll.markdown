---
layout: post
lang: nl
title:  |
    Welkom bij Jekyll! The Transformation "GREAT GOD! CAN IT BE !!"
date:   2019-04-04 13:03:33 +0200
categories: coding
# http://loc.gov/pictures/resource/cph.3g08267/ for original poster
# create a hash for this post for instance with $ md5sum <<<$(date)  on linux, of cygwin and put it in di attribute of the span element in post_id
post_id: |
    <span class="post_comment" data-postid="d7b9eac5a628596b6b44db1811073b78">
    </span>
---
<p></p>

![The transformation "GREAT GOD! CAN IT BE !!]({{ site.baseurl }}/images/the_transformation.jpg)

Ik probeer me te herinneren wat ik precies heb gedaan om Jekyll op windows 10 te installeren en te koppelen aan [gitlab pages](https://docs.gitlab.com/ee/user/project/pages/).  


Wat is Jekyll ook alweer? [De Jekyll-site](https://jekyllrb.com/) roept: "Transform your plain text into static websites and blogs." Een "static site generator" dus (met het idee dat de gegenereerde site weleens een blog zou kunnen zijn). (De naam "Jekyll" verwijst, neem ik aan, naar die transformatie. Onduidelijk is me welk van pre- en post-transformatie-kanten de "Jekyll"-kant en de "Hyde"-kant zijn (zie ["Dr. Jekyll and Mr. Hyde"](http://www.gutenberg.org/files/43/43-h/43-h.htm)).  Kort door de bocht: je maakt je bijdrages aan je blog door eerst bestandjes te schrijven met bijvoorbeeld [markdown](https://en.wikipedia.org/wiki/Markdown)-opmaak, op je eigenste thuiscomputer. Op die eigenste computer moet dan een commando aan jekyll gegeven worden om de bestanden van je blog die op het internet gepubliceerd gaan worden (in html), te genereren aan de hand van onder andere jouw markdown-bestanden.  


Voor het installeren van Jekyll bij m'n windows 10, kwam de jekyll-site me te hulp: [Jekyll on Windows](https://jekyllrb.com/docs/installation/windows/). 


Eerst heb ik [uru](https://bitbucket.org/jonforums/uru/) (een command-line-gereedschap voor het hanteren van verschillende versies van Ruby) geïnstalleerd: op de [downloadpagina](https://bitbucket.org/jonforums/uru/wiki/Downloads), "uru-0.8.5-windows-x86"-link gekozen, [uru-0.8.5-windows-x86.7z](https://bitbucket.org/jonforums/uru/downloads/uru-0.8.5-windows-x86.7z) daarmee gedownload, uru-0.8.5-windows-x86.7z uitgepakt en uru_rt.exe geplaatst in D:/tools (een directory die in PATH stond).  


Met PowerShell het commando  
```D:\tools> uru_rt admin add system```  
uitgevoerd: creëert uru.ps1 en uru.bat.


Verder gaand met [Jekyll on Windows](https://jekyllrb.com/docs/installation/windows/) gekozen voor de "Installation via RubyInstaller"-optie. De [download-pagina](https://rubyinstaller.org/downloads/) voor RubyInstaller raadde me de  Ruby+Devkit 2.5.X (x64)-versie aan. Zo gezegd, zo gedaan:  RubyInstaller gedownload en uitgevoerd, inclusief de ridk-installeerstap. Resultaat bij mij: D:\rubies\Ruby25-x64. Vervolgens deze Ruby geregistreerd bij uru en geactiveerd ...met windows powershell:  
```uru admin add D:\rubies\Ruby25-x64\bin```  
Daarna  
``` uru ls```  
```=> 253p105     : ruby 2.5.3p105 (2018-10-18 revision 65156) [x64-mingw32]```  
en  
```uru 253p105```  
waarna Ruby-commando werkte:  
```ruby -v```  
```=> ruby 2.5.3p105 (2018-10-18 revision 65156) [x64-mingw32]```  


En verder volgens [Jekyll on Windows via RubyInstaller](https://jekyllrb.com/docs/installation/windows/#installation-via-rubyinstaller):  
```gem install jekyll bundler```  
(gem zijnde een package-manager voor Ruby, jekyll en bundler van die packages mag ik aannemen:-).)  
```jekyll -v```  
```=> jekyll 3.8.5```  
liet zien dat dat iets had opgeleverd.  


Met [`jekyll new words_from_ceo`](https://jekyllrb.com/docs/usage/) kon ik daarna een standaard-jekyll-project aanmaken. 
Door bestanden aan te maken in de _posts-directory met een inhoud bestaande uit een mengel van markdown, Liquid (een template-taaltje) en yaml, 
een "build"-stap uit te voeren op de command-line, wordt de site, onder andere aan de hand van de _post-bestanden,  aangemaakt in de _site-directory. 
Met 
```jekyll serve```  
(in de directory van het project) kan dan een locale server geactiveerd worden die de site "opdient".  



De _site-directory kan dan onder git-beheer gesteld worden, en naar gitlab gepushed worden. Als er dan ook een .gitlab-ci.yml -bestand met geschikte inhoud aanwezig is, wordt de site automatisch gepubliceerd op gitlab.io ([gitlab pages](https://docs.gitlab.com/ee/user/project/pages/) heet dat). In dit geval op [https://napnopnet.gitlab.io/words-from-our-ceo/](https://napnopnet.gitlab.io/words-from-our-ceo/). Om de jekyll-product-site geschikt te maken voor de gebruikte url op gitlab, moest in het _config.yml -bestand nog een 'baseurl: "/words-from-our-ceo"'-regel toegevoegd worden. En er moet opgelet worden dat een echte `jekyll build`-stap wordt gebruikt voorafgaand aan pushen naar gitlab (en niet alleen een `jekyll serve`-stap), om te voorkomen dat er absolute locale url's in de code terecht komen.  


Omdat het een Nederlandstalige site betreft, heb ik een eenvoudige oplossing voor localisatie naar Nederlands gebruikt: lees de README.md van  [https://github.com/nelsonsar/jekyll-i18n-filter/](https://github.com/nelsonsar/jekyll-i18n-filter/). Dus i18n_filter.rb opgehaald en in _plugins-directory geplaatst en de "LOCALE = 'lv'" aangepast naar "LOCALE = 'nl'". Daarna een _locales-directory aangemaakt en de [nl-locale nl.yml](https://raw.githubusercontent.com/svenfuchs/rails-i18n/master/rails/locale/nl.yml) daarin geplaatst. Daarna was het mogelijk de dagen van de week, de datums etc. op z'n Nederlands weer te geven.   


[![Poster Dr. Jekyll and Mr. Hyde]({{ site.baseurl }}/images/800px-Dr_Jekyll_and_Mr_Hyde_poster.png)]({{ site.baseurl }}/images/DrJekyllAndMrHydePoster.jpg)


![Cover of The Stange Case of Dr. Jekyll and mr. Hyde]({{ site.baseurl }}/images/googleJekyllAndHyde.jpg "een groot paneel")
<br />
<br />
{{ page.post_id }}
