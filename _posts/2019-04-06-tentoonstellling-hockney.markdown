---
layout: post
lang: nl
title:  "Tentoonstelling Hokney"
date:   2019-04-06 13:03:33 +0200
categories: museum-visit

# create a hash for this post for instance with $ md5sum <<<$(date)  on linux, of cygwin and put it in di attribute of the span element in post_id
post_id: |
    <span class="post_comment" data-postid="9eaf8abab3231dfbeb80b18b4a552575">
    </span>
---

<p></p> 
Een indrukwekkende tijdelijke tentoonstelling in het Van Gogh-museum, waar werken van David Hockney (Engelse landschappen) worden getoond en vergeleken met schilderijen van Van Gogh. Ook is er een zaal met een viertal wanden bestaande uit talrijke monitoren waarop hypnotiserende bewegingen door landschappen worden getoond.   

Interessant is dat bij een aantal werken van grote omvang, de beperkte afstand die de waarnemer kan nemen, veroorzaakt dat de waarnemer niet direct merkt dat er meerdere perspectieven in de werken aanwezig zijn. 

<!-- ![an image alt text]({{ site.baseurl }}/images/groot-paneel.webp "een groot paneel") -->

[![an image alt text]({{ site.baseurl }}/images/groot-paneel.webp "een groot paneel")]({{ site.baseurl }}/images/groot-paneel.webp)

<!-- [David Hockney video installation](https://www.youtube.com/watch?v=vgWPQpmVcQs) -->
<!-- [plaatje]({{ site.baseurl }}/images/groot-paneel.webp) -->
{% comment %} iframes for youtube are slow to load: load "loader" iframe first and {% endcomment %}
{% comment %} after pageload event replace iframe attributes of iframes with class deferred-iframe {% endcomment %}
<style>iframe {--width-max: 90vw;max-width:var(--width-max);max-height:calc(var(--width-max)* 0.5625);}</style>
<iframe class="deferred-iframe" data-replace-src="https://www.youtube.com/embed/vgWPQpmVcQs" data-add-allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" data-add-allowfullscreen="allowfullscreen" style="width:48em;height:27em" src="{{ site.baseurl }}/blocker.html" frameborder="0"></iframe>
<iframe class="deferred-iframe" data-replace-src="https://www.youtube.com/embed/Cdqch3-D94A" data-add-allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" data-add-allowfullscreen="allowfullscreen" style="width:48em;height:27em" src="{{ site.baseurl }}/blocker.html" frameborder="0"></iframe>
<br /><br />
{{ page.post_id }}
